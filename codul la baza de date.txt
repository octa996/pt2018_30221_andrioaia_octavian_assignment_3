-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema depozit
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema depozit
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `depozit` DEFAULT CHARACTER SET utf8 ;
USE `depozit` ;

-- -----------------------------------------------------
-- Table `depozit`.`client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `depozit`.`client` (
  `idClient` INT(11) NOT NULL AUTO_INCREMENT,
  `nume` VARCHAR(45) NOT NULL,
  `prenume` VARCHAR(45) NOT NULL,
  `varsta` INT(11) NOT NULL,
  `adresa` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idClient`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `depozit`.`stoc`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `depozit`.`stoc` (
  `idStoc` INT(11) NOT NULL AUTO_INCREMENT,
  `unitatiDisponibile` INT(11) NOT NULL,
  PRIMARY KEY (`idStoc`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `depozit`.`produs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `depozit`.`produs` (
  `idProdus` INT(11) NOT NULL AUTO_INCREMENT,
  `nume` VARCHAR(45) NOT NULL,
  `pret` INT(11) NOT NULL,
  `Stoc_idStoc` INT(11) NOT NULL,
  PRIMARY KEY (`idProdus`, `Stoc_idStoc`),
  INDEX `fk_Produs_Stoc1_idx` (`Stoc_idStoc` ASC),
  CONSTRAINT `fk_Produs_Stoc1`
    FOREIGN KEY (`Stoc_idStoc`)
    REFERENCES `depozit`.`stoc` (`idStoc`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 13
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `depozit`.`comanda`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `depozit`.`comanda` (
  `idComanda` INT(11) NOT NULL AUTO_INCREMENT,
  `data` VARCHAR(45) NOT NULL,
  `Client_idClient` INT(11) NOT NULL,
  `Produs_idProdus` INT(11) NOT NULL,
  PRIMARY KEY (`idComanda`, `Client_idClient`, `Produs_idProdus`),
  INDEX `fk_Comanda_Client_idx` (`Client_idClient` ASC),
  INDEX `fk_Comanda_Produs1_idx` (`Produs_idProdus` ASC),
  CONSTRAINT `fk_Comanda_Client`
    FOREIGN KEY (`Client_idClient`)
    REFERENCES `depozit`.`client` (`idClient`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Comanda_Produs1`
    FOREIGN KEY (`Produs_idProdus`)
    REFERENCES `depozit`.`produs` (`idProdus`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

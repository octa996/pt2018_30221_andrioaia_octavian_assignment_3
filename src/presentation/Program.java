package presentation;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.TextArea;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import bll.ClientBLL;
import bll.ProdusBLL;
import model.Client;
import model.Comanda;
import dao.ClientDAO;
import dao.ComandaDAO;
import model.Produs;
import reflection.ReflectionExample;
import dao.ProdusDAO;


public class Program extends JFrame {
	
	JTextField tnume,tprenume,tani,tadresa;
	JTextField pnume,ppret,pstoc;
	JTextField tsearch1,tsearch2;
	JTextField rez1,rez2,rez3,rez5;
	String cautareclient,cautareprodus;
	String clientnume,clientprenume,clientadresa,clientvarsta;
	String produsnume,produspret,produstoc;
	String rezafisare5;
	Client client;
	Produs produs;
	JTable tabelclienti=new JTable();
	JTable tableproduse=new JTable();
	JTable tabelcomenzi=new JTable();
	String[] coloanaclienti= {
			"id","nume","prenume","varsta","adresa"
	};
	String[] coloanaproduse= {
			"id","nume","pret","id_stoc"
	};
	String[] coloanacomenzi= {
			"id","data","idclient","idprodus"
	};
	
	String datacomanda,clientid,produsid;
	
	JTextField comanda1,comanda2,comanda3;
	
	
	
	
	
	public Program() throws FileNotFoundException, UnsupportedEncodingException
	{
		
		
		JButton butonclient=new JButton("Inserare Client");
		JButton butonprodus=new JButton("Inserare Produs");
		JButton butonsclient=new JButton("Cauta Client");
		JButton butonsprodus=new JButton("Cauta Produs");
		JButton butonafisarep=new JButton("Afisare produse");
		JButton butonafisarec=new JButton("Afisare clienti");
		JButton butonchitanta=new JButton("Creare Chitanta");
		JButton butoncomanda= new JButton("Inserare Comanda");
		JButton butonafisareco=new JButton("Afisare comenzi");
		
		
		JPanel panel3=new JPanel();
		
		panel3.add(butonclient);
		panel3.add(butonprodus);
		panel3.add(butonsprodus);
		panel3.add(butonsclient);
		panel3.add(butonafisarep);
		panel3.add(butonafisarec);
		panel3.add(butonchitanta);
		panel3.add(butoncomanda);
		panel3.add(butonafisareco);
		
		butonafisareco.addActionListener((e)->
		{
			try {
				actiune9();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		
		butoncomanda.addActionListener((e)->
		{
			actiune8();
			
			
		});
		
		
		
		
		
		JPanel panelcomanda=new JPanel();
		
		JLabel labelcomanda1=new JLabel("Introduceti data");
		comanda1=new JTextField("",15);
		
		JLabel labelcomanda2=new JLabel("Introduceti id-ul clientului");
		comanda2=new JTextField("",15);
		
		JLabel labelcomanda3=new JLabel("Introduceti id-ul produsului");
		
		comanda3=new JTextField("",15);
		
		panelcomanda.add(labelcomanda1);
		panelcomanda.add(comanda1);
		panelcomanda.add(labelcomanda2);
		panelcomanda.add(comanda2);
		panelcomanda.add(labelcomanda3);
		panelcomanda.add(comanda3);
		
		
		
		
		
		
		
		
		butonchitanta.addActionListener((e)->
		{
			try {
				actiune7();
			} catch (FileNotFoundException | UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		});
		
		
		
		
		butonafisarec.addActionListener((e)->
		{
			try {
				actiune6();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		
		
		butonafisarep.addActionListener((e)->
		{
			try {
				actiune5();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		
		butonclient.addActionListener((e)->{
			actiune1();
		});
		
		butonprodus.addActionListener((e)->{
			actiune2();
		});
		
		butonsprodus.addActionListener((e)->{
			actiune3();
			
		});
		butonsclient.addActionListener((e)->{
			actiune4();
		});
		
		
		
		
		
		
		
		
		
		
		 JFrame jf = new JFrame();
		
		 
		 JPanel panel =new JPanel();
		 
		 
		 
		 JPanel panelrez=new JPanel();
		 JPanel panelrez1=new JPanel();
		 rez1=new JTextField("",50);
		 rez2=new JTextField("",50);
		 rez3=new JTextField("",50);
		 
		 panelrez.add(rez1);
		 panelrez.add(rez2);
		 panelrez1.add(rez3);
		 
		JPanel panel1=new JPanel();
		
		
		
		
		JPanel panel12=new JPanel();
		JLabel label11=new JLabel("Adaugati Clientul");
		panel12.add(label11);
		
		
		JPanel panelnume=new JPanel();
		tnume= new JTextField("",15);
		JLabel lnume=new JLabel("Introduceti numele");
		
		panelnume.add(lnume);
		panelnume.add(tnume);
		
		
		JPanel panelprenume=new JPanel();
		tprenume=new JTextField("",15);
		JLabel lprenume=new JLabel("Introduceti prenumele");
		
		panelprenume.add(lprenume);
		panelprenume.add(tprenume);
		
		
		JPanel panelani=new JPanel();
		tani=new JTextField("",15);
		JLabel lani=new JLabel("Introduceti varsta");
		
		panelani.add(lani);
		panelani.add(tani);
		
		
		JPanel paneladresa=new JPanel();
		tadresa=new JTextField("",15);
		JLabel ladresa=new JLabel("Introduceti adresa");
		
		
		paneladresa.add(ladresa);
		paneladresa.add(tadresa);
		
		
	
		panel1.add(panel12);
		panel1.add(panelnume);
		panel1.add(panelprenume);
		panel1.add(panelani);
		panel1.add(paneladresa);
		
		
	
		
		
		
		
		
		
		
JPanel panel2=new JPanel();
		
		
		JLabel label21=new JLabel("Adaugare Produs");
		panel2.add(label21);	
		
		
		JPanel panelpnume=new JPanel();
		JLabel labelpnume=new JLabel("Introduceti numele");
		pnume=new JTextField("",15);
		
		panelpnume.add(labelpnume);
		panelpnume.add(pnume);
		
		
		
		
		JPanel panelpret=new JPanel();
		JLabel labelpret=new JLabel("Introduceti pret");
		ppret=new JTextField("",15);
		
		panelpret.add(labelpret);
		panelpret.add(ppret);
		
		
		JPanel panelstoc=new JPanel();
		JLabel labelstoc=new JLabel("Indtroduceti id-ul stoc-ului");
		pstoc= new JTextField("",15);
		
		panelstoc.add(labelstoc);
		panelstoc.add(pstoc);
		
		
		panel2.add(panelpnume);
		panel2.add(panelpret);
		panel2.add(panelstoc);
		
		
		
		JPanel panelsearch1=new JPanel();
		JLabel labelsearch1=new JLabel("Cauta produs");
		tsearch1=new JTextField("",15);
		
		panelsearch1.add(labelsearch1);
		panelsearch1.add(tsearch1);
		
		
		
		panel2.add(panelsearch1);
		
		JPanel panelsearch2=new JPanel();
		JLabel labelsearch2=new JLabel("Cauta client");
		tsearch2=new JTextField("",15);
		
		panelsearch2.add(labelsearch2);
		panelsearch2.add(tsearch2);
		
		
		

		rez5=new JTextField("",40);
		panel2.add(panelsearch2);
		
		panelrez1.add(rez5);
		
		JPanel tabele=new JPanel();
		
	
	 
		
		panel.add(panel1);
		panel.add(panel2);
		panel.add(panel3);
		panel.add(panelrez);
		panel.add(panelcomanda);
		
		

		jf.add(panel);
		
		 jf.setSize(800, 200);
		    jf.setVisible(true);
		    jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    

	}
	
	private void actiune9() throws SQLException, IOException {
		// TODO Auto-generated method stub
		ComandaDAO co=new ComandaDAO();
		ArrayList<Object> lista=new ArrayList<Object>();
		lista=co.afisaretot();
		Object data[][]=new Object[1000][1000];
		int j=lista.size()/4;
		int x=j;
		int l=0;
		for(int i=0;i<j;i++)
		{
			int k=0;
			while(k<4)
			{
				data[i][k]=lista.get(l);
				k++;
				l++;
			}
		}
		
		l=0;
		FileWriter writer1 = new FileWriter("MyFile.txt", true);
		for(int i=0;i<j;i++)
		{
			int k=0;
			while(k<4)
			{
				data[i][k]=lista.get(l);
				k++;
				l++;
				String aux=(String) lista.get(l);
				writer1.write(aux+" ");
			}
			
		}
		writer1.close();
		
	}

	private void actiune8() {
		datacomanda=comanda1.getText();
		clientid=comanda2.getText();
		produsid=comanda3.getText();
		
		Comanda comanda=new Comanda(datacomanda,Integer.parseInt(clientid),Integer.parseInt(produsid));
		ComandaDAO dao=new ComandaDAO();
		dao.insert(comanda);
		
	}

	private void actiune7() throws FileNotFoundException, UnsupportedEncodingException {
		// TODO Auto-generated method stub
		PrintWriter writer = new PrintWriter("the-file-name.txt", "UTF-8");
		
		writer.println("Clientul cu numele "+clientnume+" "+clientprenume+" "+"a achizitionat");
		writer.println(produstoc +" "+produsnume+" in valoare de "+produspret+" lei");
		writer.close();
	}

	private void actiune6()throws SQLException {
		// TODO Auto-generated method stub
		ClientDAO ccc1=new ClientDAO();
		ArrayList<Client> lista=new ArrayList<Client>();
		lista=ccc1.afisarereflex();
		Object data[][] = new Object[1000][1000];
		ReflectionExample e=new ReflectionExample();
		for(int i=0;i<lista.size();i++)
			e.retrieveProperties(lista.get(i),data,i);
		tabelclienti=new JTable(data,coloanaclienti);
		JScrollPane sp=new JScrollPane(tabelclienti);
		JFrame f = new JFrame();
		f.add(sp);
		f.setVisible(true);
		
	}

	private void actiune5() throws SQLException {
		ProdusDAO ppp1=new ProdusDAO();
		ArrayList<Object> lista=new ArrayList<Object>();
		lista=ppp1.afisaretot();
		Object data[][] = new Object[1000][1000];
		int j=lista.size()/4;
		int x=j;
		System.out.println(j);
		int l=0;
		for(int i=0;i<j;i++)
		{
			int k=0;
			
			while(k<4)
			{
				data[i][k]=lista.get(l);
				l++;
				k++;
			}
		}
		

		tableproduse=new JTable(data,coloanaproduse);
		JScrollPane sp=new JScrollPane(tableproduse);
		JFrame f = new JFrame();
		f.add(sp);
		f.setVisible(true);
		
		}
	

	private void actiune4() {
		// TODO Auto-generated method stub
		ClientDAO cc1=new ClientDAO();
		cautareclient=tsearch2.getText();
		Client c1=cc1.gasesteDupaID(Integer.parseInt(cautareclient));
		rez1.setText(c1.toString());
	}

	private void actiune3() {
		
		ProdusDAO pp1=new ProdusDAO();
		cautareprodus=tsearch1.getText();
		Produs p1=pp1.findById(Integer.parseInt(cautareprodus));
		rez2.setText(p1.toString());
		
		
	}

	private void actiune2() {
		produsnume=pnume.getText();
		produspret=ppret.getText();
		produstoc=pstoc.getText();
		
		Produs produs=new Produs(produsnume,Integer.parseInt(produspret),Integer.parseInt(produstoc));
		ProdusBLL produsbll=new ProdusBLL();
		int id=produsbll.insert(produs);
		
		
	}

	private void actiune1() {
		
		clientnume=tnume.getText();
		clientprenume=tprenume.getText();
		clientadresa=tadresa.getText();
		clientvarsta=tani.getText();
		
		Client client=new Client(clientnume,clientprenume,Integer.parseInt(clientvarsta),clientadresa);
		
		ClientBLL clientbll=new ClientBLL();
		int id=clientbll.insereaza(client);
		
		
	}

	


}

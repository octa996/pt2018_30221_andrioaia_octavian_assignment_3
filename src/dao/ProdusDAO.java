package dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Produs;

public class ProdusDAO {
	
	protected static final Logger LOGGER = Logger.getLogger(ProdusDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO produs (nume,pret,Stoc_idStoc)"
			+ " VALUES (?,?,?)";
	private final static String findStatementString = "SELECT * FROM produs where idProdus = ?";
	private final static String findall="SELECT * FROM produs";
	public static ArrayList afisaretot() throws SQLException
	{
		ArrayList<Object> lista=new ArrayList<Object>();
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement finStatement=dbConnection.prepareStatement("SELECT * from produs");
		
		ResultSet rs=null;
		try
		{
			rs=finStatement.executeQuery();
			
			while(rs.next())
			{
				lista.add(new Integer(rs.getInt(1)));
				lista.add(rs.getString(2));
				lista.add(new Integer(rs.getInt(3)));
				lista.add(new Integer(rs.getInt(4)));
				
			}
		}
		catch(SQLException e)
		{
			System.out.println(e.getMessage());
		}

		return lista;
	}
	
	
	
	public static Produs findById(int idProdus) {
		Produs toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, idProdus);
			rs = findStatement.executeQuery();
			rs.next();

			String nume = rs.getString("nume");
			int pret=rs.getInt("pret");
			int Stoc_idStoc=rs.getInt("Stoc_idStoc");
			toReturn=new Produs(idProdus,nume,pret,Stoc_idStoc);
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProdusDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	public static int insert(Produs produs) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1,produs.getNume());
			insertStatement.setInt(2, produs.getPret());
			insertStatement.setInt(3, produs.getStoc_idStoc());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
}

package dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Client;

public class ClientDAO {
	
	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO Client (nume,prenume,varsta,adresa)"
			+ " VALUES (?,?,?,?)";
	private final static String findStatementString = "SELECT * FROM client where idClient = ?";
	
	public static ArrayList<Client> afisarereflex()
	{
		ArrayList<Client>lista=new ArrayList<Client>();
		Client rezultat= null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs=null;
		for(int i=8;i<30;i++)
		{
		try
		{
			findStatement=dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, i);
			rs=findStatement.executeQuery();
			rs.next();
			
			
			String nume=rs.getString("nume");
			String prenume=rs.getString("prenume");
			int varsta=rs.getInt("varsta");
			String adresa=rs.getString("adresa");
			rezultat=new Client(i,nume,prenume,varsta,adresa);
			lista.add(rezultat);
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
		} 	
		}

		ConnectionFactory.close(rs);
		ConnectionFactory.close(findStatement);
		ConnectionFactory.close(dbConnection);

		return lista;
		
		
		
		
	}
	
	
	public static ArrayList afisaretot() throws SQLException
	{
		ArrayList<Object> lista=new ArrayList<Object>();
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement finStatement=dbConnection.prepareStatement("SELECT * from client");
		
		ResultSet rs=null;
		try
		{
			rs=finStatement.executeQuery();
			
			while(rs.next())
			{
				lista.add(new Integer(rs.getInt(1)));
				lista.add(rs.getString(2));
				lista.add(rs.getString(3));
				lista.add(new Integer(rs.getInt(4)));
				lista.add(rs.getString(5));
				
			}
		}
		catch(SQLException e)
		{
			System.out.println(e.getMessage());
		}

		return lista;
	}
	
	
	
	public static Client gasesteDupaID(int idClient)
	{
		Client rezultat= null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs=null;
		
		try
		{
			findStatement=dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, idClient);
			rs=findStatement.executeQuery();
			rs.next();
			
			
			String nume=rs.getString("nume");
			String prenume=rs.getString("prenume");
			int varsta=rs.getInt("varsta");
			String adresa=rs.getString("adresa");
			rezultat=new Client(idClient,nume,prenume,varsta,adresa);
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return rezultat;
	}
	
	
	
	public static int insereaza(Client client)
	{
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, client.getNume());
			insertStatement.setString(2, client.getPrenume());
			insertStatement.setInt(3, client.getVarsta());
			insertStatement.setString(4, client.getAdresa());
			insertStatement.executeUpdate();
			ResultSet rs=insertStatement.getGeneratedKeys();
			if(rs.next())
			{
				insertedId=rs.getInt(1);
				
			}
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
}
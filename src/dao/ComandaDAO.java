package dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Comanda;
public class ComandaDAO {
	
	protected static final Logger LOGGER = Logger.getLogger(ComandaDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO comanda (data,Client_idClient,Produs_idProdus)"
			+ " VALUES (?,?,?)";
	private final static String findStatementString = "SELECT * FROM comanda where id = ?";
	private final static String findall="SELECT * from comanda";
	
	public static ArrayList afisaretot() throws SQLException
	{
		ArrayList<Object> lista=new ArrayList<Object>();
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement finStatement=dbConnection.prepareStatement("SELECT * from comanda");
		
		ResultSet rs=null;
		try
		{
			rs=finStatement.executeQuery();
			
			while(rs.next())
			{
				lista.add(new Integer(rs.getInt(1)));
				lista.add(rs.getString(2));
				lista.add(new Integer(rs.getInt(3)));
				lista.add(new Integer(rs.getInt(4)));
				
			}
		}
		catch(SQLException e)
		{
			System.out.println(e.getMessage());
		}

		return lista;
	}
	
	public static Comanda findById(int idComanda) {
		Comanda toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, idComanda);
			rs = findStatement.executeQuery();
			rs.next();
			
			String data=rs.getString("data");
			int Client_idClient=rs.getInt("Client_idClient");
			int Produs_idProdus=rs.getInt("Produs_idProdus");
			toReturn=new Comanda(idComanda,data,Client_idClient,Produs_idProdus);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ComandaDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	public static int insert(Comanda comanda) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, comanda.getData());
			insertStatement.setInt(2, comanda.getClient_idClient());
			insertStatement.setInt(3, comanda.getProdus_idProdus());
			insertStatement.executeUpdate();
			
			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ComandaDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
}
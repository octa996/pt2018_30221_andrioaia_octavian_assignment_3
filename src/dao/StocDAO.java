package dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Stoc;

public class StocDAO {
	
	protected static final Logger LOGGER = Logger.getLogger(StocDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO stoc (unitatiDisponibile)"
			+ " VALUES (?,?,?,?)";
	private final static String findStatementString = "SELECT * FROM stoc where id = ?";
	
	public static Stoc findById(int idStoc) {
		Stoc rezultat = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try
		{
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, idStoc);
			rs = findStatement.executeQuery();
			rs.next();
			int unitatiDisponibile=rs.getInt("unitatiDisponibile");
			rezultat =new Stoc(idStoc,unitatiDisponibile);
		}
			catch (SQLException e) {
				LOGGER.log(Level.WARNING,"StudentDAO:findById " + e.getMessage());
			} finally {
				ConnectionFactory.close(rs);
				ConnectionFactory.close(findStatement);
				ConnectionFactory.close(dbConnection);
			}
			return rezultat;
		}
		
	
	public static int insert(Stoc stoc)
	{
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		  
		try
		{
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, stoc.getUnitatiDisponibile());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "StocDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
}
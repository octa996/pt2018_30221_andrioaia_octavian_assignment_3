package reflection;

import java.lang.reflect.Field;

public class ReflectionExample {
	public static void retrieveProperties(Object object,Object data[][],int i)
	{
		int j=0;
		for(Field field :object.getClass().getDeclaredFields())
		{
			
			field.setAccessible(true);
			Object value;
			try
			{
				value=field.get(object);
				System.out.println(field.getName()+ " "+value);
			data[i][j]=value;
				
			}
			catch (IllegalArgumentException e)
			{
				e.printStackTrace();
			}catch (IllegalAccessException e)
			{
				e.printStackTrace();
			}
			j++;
		}
	}
	

}

package model;

public class Client {

	private int idClient;
	private String nume;
	private String prenume;
	private int varsta;
	private String adresa;
	
	public Client(int idClient,String nume,String prenume,int varsta,String adresa)
	{
		super();
		this.idClient=idClient;
		this.nume=nume;
		this.prenume=prenume;
		this.varsta=varsta;
		this.adresa=adresa;
	}
	
	
	public Client( String nume,String prenume,int varsta,String adresa)
	{
		super();
		
		this.nume=nume;
		this.prenume=prenume;
		this.varsta=varsta;
		this.adresa=adresa;
	}

	public Client() {
		// TODO Auto-generated constructor stub
	}


	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getPrenume() {
		return prenume;
	}

	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}

	public int getVarsta() {
		return varsta;
	}

	public void setVarsta(int varsta) {
		this.varsta = varsta;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	
	
	public String toString() {
		return "Client [id=" + idClient + ", nume=" + nume + ", prenume=" + prenume + ", adresa=" + adresa + ", varsta=" + varsta
				+ "]";
	}
	
	
}

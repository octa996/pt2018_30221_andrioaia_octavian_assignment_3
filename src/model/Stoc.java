package model;

public class Stoc {
	
	private int idStoc;
	private int unitatiDisponibile;
	
	public Stoc(int idStoc,int unitatiDisponibile)
	{
		this.idStoc=idStoc;
		this.unitatiDisponibile=unitatiDisponibile;
	}

	public int getIdStoc() {
		return idStoc;
	}

	public void setIdStoc(int idStoc) {
		this.idStoc = idStoc;
	}

	public int getUnitatiDisponibile() {
		return unitatiDisponibile;
	}

	public void setUnitatiDisponibile(int unitatiDisponibile) {
		this.unitatiDisponibile = unitatiDisponibile;
	}

	
	public String toString()
	{
		return "Stoc [id=" + idStoc + " unitatiDisponibile="+unitatiDisponibile+"]";
	}
	
}

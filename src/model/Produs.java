package model;

public class Produs {
	private int idProdus;
	private String nume;
	private int pret;
	private int Stoc_idStoc;

	public Produs(int idProdus,String nume,int pret,int Stoc_idStoc)
	{
		this.idProdus=idProdus;
		this.nume=nume;
		this.pret=pret;
		this.Stoc_idStoc=Stoc_idStoc;
		
	}
	

	public Produs(String nume, int pret, int stoc_idStoc) {
		super();
		this.nume = nume;
		this.pret = pret;
		Stoc_idStoc = stoc_idStoc;
	}


	public int getIdProdus() {
		return idProdus;
	}

	public void setIdProdus(int idProdus) {
		this.idProdus = idProdus;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public int getPret() {
		return pret;
	}

	public void setPret(int pret) {
		this.pret = pret;
	}

	public int getStoc_idStoc() {
		return Stoc_idStoc;
	}

	public void setStoc_idStoc(int stoc_idStoc) {
		Stoc_idStoc = stoc_idStoc;
	}
	
	public String toString() {
		return "Produs [id=" + idProdus + ", nume=" + nume + ", pret=" + pret + ", Stoc_idStoc=" + Stoc_idStoc 
				+ "]"+"";
	}
	
	
}

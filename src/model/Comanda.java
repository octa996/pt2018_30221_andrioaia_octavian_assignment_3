package model;
public class Comanda {
	
	private int idComanda;
	private String data;
	private int Client_idClient;
	private int Produs_idProdus;
	
	public Comanda(int idComanda,String data,int Client_idClient,int Produs_idProdus)
	{
		this.idComanda=idComanda;
		this.data=data;
		this.Produs_idProdus=Produs_idProdus;
		this.Client_idClient=Client_idClient;
		
	}

	

	public Comanda(String data, int client_idClient, int produs_idProdus) {
		super();
		this.data = data;
		Client_idClient = client_idClient;
		Produs_idProdus = produs_idProdus;
	}



	public int getIdComanda() {
		return idComanda;
	}

	public void setIdComanda(int idComanda) {
		this.idComanda = idComanda;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public int getClient_idClient() {
		return Client_idClient;
	}

	public void setClient_idClient(int client_idClient) {
		Client_idClient = client_idClient;
	}

	public int getProdus_idProdus() {
		return Produs_idProdus;
	}

	public void setProdus_idProdus(int produs_idProdus) {
		Produs_idProdus = produs_idProdus;
	}
	
	
	public String toString() {
		return "Comanda [id=" + idComanda + ", idProdus=" + Produs_idProdus + ", idClient=" + Client_idClient + ", data=" + data 
				+ "]";
	}

}

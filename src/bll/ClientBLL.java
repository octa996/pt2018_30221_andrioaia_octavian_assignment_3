package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.Validator;
import bll.validators.VerificareMinor;
import dao.ClientDAO;
import model.Client;

public class ClientBLL {
	
	private List<Validator<Client>>validators;
	
	public ClientBLL()
	{
		validators=new ArrayList<Validator<Client>>();
		validators.add(new VerificareMinor());
		
	}
	public int insereaza(Client client)
	{
		for(Validator<Client> v:validators)
		{
			v.validate(client);
			
		}
		return ClientDAO.insereaza(client);
	}
	
	
}

package bll.validators;

import model.Produs;

public class Gratuitate implements Validator<Produs> {

	
	public void validate(Produs p)
	{
		if(p.getPret()==0)
		{
			throw new IllegalArgumentException("Un produs nu poate sa fie gratuit");
		}
	}
}

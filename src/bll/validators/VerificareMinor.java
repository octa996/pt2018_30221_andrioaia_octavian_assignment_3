package bll.validators;

import model.Client;

public class VerificareMinor implements Validator<Client> {

	private static final int MIN=18;
	
	public void validate(Client c)
	{
		if(c.getVarsta()<MIN)
		{
			throw new IllegalArgumentException("Clientul nu este major deci nu poate inchiria unelte");
		}
	}
}

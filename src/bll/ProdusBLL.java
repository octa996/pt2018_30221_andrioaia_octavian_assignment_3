package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.Gratuitate;
import bll.validators.Validator;
import bll.validators.VerificareMinor;
import dao.ClientDAO;
import dao.ProdusDAO;
import model.Client;
import model.Produs;

public class ProdusBLL {
	
	private List<Validator<Produs>>validators;
	
	public ProdusBLL()
	{
		validators=new ArrayList<Validator<Produs>>();
		validators.add(new Gratuitate());
		
	}
	public int insert(Produs produs)
	{
		for(Validator<Produs> v:validators)
		{
			v.validate(produs);
		}
		return ProdusDAO.insert(produs);
		
	}
	
}
